# Sherman, Nolan - Assignment 3
Philosophers Dining Problem, CSCI4404, P. Vandrevu



## Question 1
For this question I implemented the random wait time based solution for the philosophers dining problem as
described in the insructions. The program prints to standard out so you can simply redirect output to a file
of your choosing. I have saved the reults to a file for your convenience. The location of the exectuable, results and the source code is listed below.

|File | Location |
|-|-|
|Source Code : |./Q1/main.go & ./Q1/philosopher.go  
|Executable : |./Q1/philosophers-dine  
|Output : |./results.Q1.txt  

### Results
Below is a table showing the time that each philosopher spent in each state and their average time spent in each state.

| Philosopher  |  Eating |  Hungry | Thinking  |
|--------------|---------|---------|-----------|
|  0           |14464 ms |39341 ms |5910 ms    |
|  1           |15095 ms |38358 ms |6291 ms    |
|  2           |13790 ms |40189 ms |5760 ms    |
|  3           |16188 ms |36852 ms |6770 ms    |
|  average     |14884 ms |38685 ms |6182 ms    |


## Question 2
For this question I implemented the textbook solution for the philosophers dining problem. 
The program prints to standard out so you can simply redirect output to a file of your choosing. 
I have saved the reults to a file for your convenience. The location of the exectuable, results 
and the source code is listed below.

|File | Location |
|-|-|
|Source Code : |./Q2/main.go & ./Q2/philosopher.go  
|Executable : |./Q2/philosophers-dine  
|Output : |./results.Q2.txt  

### Results
Below is a table showing the time that each philosopher spent in each state and their average time spent in each state.

| Philosopher  |  Eating |  Hungry | Thinking  |
|--------------|---------|---------|-----------|
|  0           |24081 ms |25211 ms |9993 ms    |
|  1           |24478 ms |24944 ms |9900 ms    |
|  2           |23933 ms |25308 ms |10070 ms   |
|  3           |24283 ms |25058 ms |9980 ms    |
|  average     |24193 ms |25130 ms |9985 ms    |


## Comparison Summary

The Hungry state of a philosopher thread is synonmous to a thread that is blocked. With that in mind, we see that
on average the threads spent ~13 more seconds (~13,000 ms) in the blocked state in the first implementation 
then compared to the more optimal solution (Q2). We also see that the more opitmal solution spent more time
working (eating and thinking states) then the less optimal solution (Q1). Which makes sense! Less time blocked
means more time working.
