module nolansherman.os.hw3

go 1.13

require (
	github.com/envoyproxy/go-control-plane v0.9.4 // indirect
	golang.org/x/net v0.0.0-20190311183353-d8887717615a
	golang.org/x/sync v0.0.0-20190423024810-112230192c58
)
