package main

import (
	"fmt"
	"log"
	"math/rand"
	"time"

	"golang.org/x/sync/semaphore"
)

const (
	logformat       = "Philosopher: %d, Time: %d ms, %s"
	hungerMessage   = "entering hungry state"
	eatingMessage   = "entering eating state"
	thinkingMessage = "entering thinking state"
)

var Continue = true

//Fork is an instance of one
//fork used by the Philosopher
type Fork struct {
	ID int
	//a semeaphore is used as a mutex here because golang sync.
	//Mutex waits indefinetly by default and there is no way to
	//check if it is locked or not. A sepmaphore with value 1
	//is equivelent to a mutex.
	Mtx *semaphore.Weighted
}

//State a Philosopher is in
type State int

const (
	//StateHunger philosopher is hungry
	StateHunger State = 100
	//StateEating philosopher is eating
	StateEating State = 101
	//StateThinking philosopher is thinking
	StateThinking State = 102
)

//Philosopher represents a dining
//Philosopher
type Philosopher struct {
	ID                 int
	ForkLeft           *Fork
	ForkRight          *Fork
	Log                *log.Logger
	CreatedAt          time.Time
	state              State
	statechangedAt     time.Time
	timeHungryMillis   int64
	timeThinkingMillis int64
	timeEatingMillis   int64
}

//Think simulates the philosophers thinking phase by
//pausing the current go routine for 10 ms
func (p *Philosopher) Think() {
	p.updateState(StateThinking)
	p.log(thinkingMessage)
	time.Sleep(10 * time.Millisecond)
}

//Wait pauses the current go routine for 50-100 ms
func (p *Philosopher) Wait() {
	t := rand.Intn(50) + 50
	time.Sleep(time.Duration(t) * time.Millisecond)
}

//Eating pauses the current go routine for 10-40 ms
func (p *Philosopher) Eating() {
	p.updateState(StateEating)
	t := rand.Intn(30) + 10
	p.log(fmt.Sprintf("entering eating state. Will eat for %d ms", t))
	time.Sleep(time.Duration(t) * time.Millisecond)
}

func (p *Philosopher) updateState(state State) {
	now := time.Now()
	oldState := p.state
	lastChanged := p.statechangedAt
	p.state = state
	p.statechangedAt = now
	//return if this is the first time a state is set.
	if lastChanged.IsZero() {
		return
	}

	elapsed := now.Sub(lastChanged).Milliseconds()
	switch oldState {
	case StateHunger:
		//If the previous state was hunger
		p.timeHungryMillis += elapsed
	case StateEating:
		//If the previous state was eating
		p.timeEatingMillis += elapsed
	case StateThinking:
		//If the previous state was thinking
		p.timeThinkingMillis += elapsed

	}
}

//Eat simulates the philosophers hunger phase.
func (p *Philosopher) Eat() {
	p.log(hungerMessage)
	p.updateState(StateHunger)
	//STEP 2. Whenever a philosopher wants to eat, he/she tries to pick up the fork to their
	//left first.
	for {
		//If available, the philosopher will pick it up (left fork).
		if p.ForkLeft.Mtx.TryAcquire(1) {
			p.log(fmt.Sprintf("picked up fork %d\n", p.ForkLeft.ID))
			//If right fork is available, the philosopher will pick it up.
			if p.ForkRight.Mtx.TryAcquire(1) {
				p.log(fmt.Sprintf("picked up fork %d\n", p.ForkRight.ID))
				//After picking up both forks, the philosopher should simulate eating
				p.Eating()
				//put down both forks after eating
				p.ForkLeft.Mtx.Release(1)
				p.ForkRight.Mtx.Release(1)
				break //we can exit the trying to eat loop.

			} else { //If not available, the philosopher will wait then try again.
				p.log(fmt.Sprintf("tried to pick up fork %d, it's unavailable, will wait and try again.\n", p.ForkRight.ID))
				p.Wait()
				//If right fork is available, the philosopher will pick it up.
				if p.ForkRight.Mtx.TryAcquire(1) {
					//After picking up both forks, the philosopher should simulate eating
					p.Eating()
					//put down both forks after eating
					p.ForkLeft.Mtx.Release(1)
					p.ForkRight.Mtx.Release(1)
					break //we can exit the trying to eat loop.

				} else { //If not, the philosopher willdrop the left fork, wait and go back to step 2.
					p.log(fmt.Sprintf("tried to pick up fork %d, it's still unavailable. Putting down both forks.\n", p.ForkRight.ID))
					p.ForkLeft.Mtx.Release(1)
					p.Wait()
					//back to step to ->
				}
			}

		} else {
			p.log(fmt.Sprintf("tried to pick up fork %d, it's unavailable\n", p.ForkLeft.ID))
			p.Wait() //wait and try again
		}
	}

}

//Dine initiates a dining Philosopher. Should be called as a thread.
//ie.. go philosopher.Dine()
func (p *Philosopher) Dine() {

	for Continue {
		//Philosophers eat first then think. Order not mentioned in instruction
		p.Eat()
		p.Think()
	}

	wg.Done()
}

func (p *Philosopher) log(message string) {
	t := time.Now().Sub(p.CreatedAt).Milliseconds()
	p.Log.Printf(logformat, p.ID, t, message)
}

//String representation of philosohopher that is his time sums.
func (p *Philosopher) String() string {
	return fmt.Sprintf("Philosopher %d's Time -- eating=%d ms, hungry=%d ms, thinking=%d ms",
		p.ID, p.timeEatingMillis, p.timeHungryMillis, p.timeThinkingMillis)
}
