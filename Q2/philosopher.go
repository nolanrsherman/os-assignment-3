package main

import (
	"fmt"
	"log"
	"math/rand"
	"sync"
	"time"
)

const (
	logformat       = "Philosopher: %d, Time: %d ms, %s"
	hungerMessage   = "entering hungry state"
	eatingMessage   = "entering eating state"
	thinkingMessage = "entering thinking state"
)

var (
	Continue = true
	//Mtx for changing states
	Mtx sync.Mutex
)

//State a Philosopher is in
type State int

const (
	//StateHunger philosopher is hungry
	StateHunger State = 100
	//StateEating philosopher is eating
	StateEating State = 101
	//StateThinking philosopher is thinking
	StateThinking State = 102
)

//Philosopher represents a dining
//Philosopher
type Philosopher struct {
	ID                 int
	Log                *log.Logger
	CreatedAt          time.Time
	state              State
	statechangedAt     time.Time
	timeHungryMillis   int64
	timeThinkingMillis int64
	timeEatingMillis   int64
	Left               *Philosopher
	Right              *Philosopher
	s                  sync.Mutex
}

//Think simulates the philosophers thinking phase by
//pausing the current go routine for 10 ms
func (p *Philosopher) Think() {
	p.log(thinkingMessage)
	time.Sleep(10 * time.Millisecond)
}

//Wait pauses the current go routine for 50-100 ms
func (p *Philosopher) Wait() {
	t := rand.Intn(50) + 50
	time.Sleep(time.Duration(t) * time.Millisecond)
}

//Eating pauses the current go routine for 10-40 ms
func (p *Philosopher) Eating() {
	t := rand.Intn(30) + 10
	p.log(fmt.Sprintf("entering eating state. Will eat for %d ms", t))
	time.Sleep(time.Duration(t) * time.Millisecond)
}

func (p *Philosopher) updateState(state State) {
	now := time.Now()
	oldState := p.state
	lastChanged := p.statechangedAt
	p.state = state
	p.statechangedAt = now
	//return if this is the first time a state is set.
	if lastChanged.IsZero() {
		return
	}

	elapsed := now.Sub(lastChanged).Milliseconds()
	switch oldState {
	case StateHunger:
		//If the previous state was hunger
		p.timeHungryMillis += elapsed
	case StateEating:
		//If the previous state was eating
		p.timeEatingMillis += elapsed
	case StateThinking:
		//If the previous state was thinking
		p.timeThinkingMillis += elapsed

	}
}

//Dine initiates a dining Philosopher. Should be called as a thread.
//ie.. go philosopher.Dine()
func (p *Philosopher) Dine() {

	p.updateState(StateThinking) //start thinking

	for Continue { /*repeat forever*/
		/*philosopher is thinking*/
		p.Think() //log think message and wait

		/*acquire two forks or block*/
		takeForks(p)

		/*yum-yum, spaghetti*/
		p.Eating()

		/*put both forks back on table*/
		/*check if neighbor can eat*/
		putForks(p)
	}

	wg.Done()
}

func takeForks(p *Philosopher) {
	/*acquire two forks or block*/

	Mtx.Lock() //enter critical region
	p.log(hungerMessage)
	p.updateState(StateHunger) //update state to hungry
	block := true
	//try to take forks
	if p.Left.state != StateEating && p.Right.state != StateEating {
		p.updateState(StateEating) //update state to eating
		block = false
	}
	Mtx.Unlock() //exit critical region

	if block { //block if could not take forks.
		p.s.Lock()
	}
}

func putForks(p *Philosopher) {
	Mtx.Lock() //enter critical region

	p.updateState(StateThinking)

	//See if left neighbor can eat
	tapNeighbor(p.Left)

	//See if right neighbor can eat
	tapNeighbor(p.Right)

	Mtx.Unlock() //exit critical region

}

func tapNeighbor(p *Philosopher) {
	if p.state == StateHunger && p.Left.state != StateEating && p.Right.state != StateEating {
		p.updateState(StateEating) //update state to eating
		p.s.Unlock()
	}
}

func (p *Philosopher) log(message string) {
	t := time.Now().Sub(p.CreatedAt).Milliseconds()
	p.Log.Printf(logformat, p.ID, t, message)
}

//String representation of philosohopher that is his time sums.
func (p *Philosopher) String() string {
	return fmt.Sprintf("Philosopher %d's Time -- eating=%d ms, hungry=%d ms, thinking=%d ms",
		p.ID, p.timeEatingMillis, p.timeHungryMillis, p.timeThinkingMillis)
}
