/**
* Operating Systems Assignment 3 - Q2
* Philosphers Dining Problem
* Author: Nolan Sherman
* Date: 3/29/2020
 */
package main

import (
	"log"
	"os"
	"sync"
	"time"
)

var ProgramLengthSeconds = 60
var wg sync.WaitGroup

func main() {
	logger := log.New(os.Stdout, "O.S Assignment 3 Q2: ", 0)
	logger.Printf("Philosohper dinner party started")

	//Create philosopgers
	p0 := &Philosopher{
		ID:        0,
		Log:       logger,
		CreatedAt: time.Now(),
	}

	p1 := &Philosopher{
		ID:        1,
		Log:       logger,
		CreatedAt: time.Now(),
	}

	p2 := &Philosopher{
		ID:        2,
		Log:       logger,
		CreatedAt: time.Now(),
	}

	p3 := &Philosopher{
		ID:        3,
		Log:       logger,
		CreatedAt: time.Now(),
	}

	p0.Right = p3
	p0.Left = p1
	p1.Right = p0
	p1.Left = p2
	p2.Right = p1
	p2.Left = p3
	p3.Right = p2
	p3.Left = p0

	//begin dining
	go p0.Dine()
	go p1.Dine()
	go p2.Dine()
	go p3.Dine()
	wg.Add(4)
	time.Sleep(time.Duration(ProgramLengthSeconds) * time.Second) //stop after 60 seconds
	Continue = false
	wg.Wait() //wait for all to finish.

	logger.Println(p0)
	logger.Println(p1)
	logger.Println(p2)
	logger.Println(p3)

	sum := p0.timeEatingMillis + p1.timeEatingMillis + p2.timeEatingMillis + p3.timeEatingMillis
	eatAvg := sum / 4

	sum = p0.timeHungryMillis + p1.timeHungryMillis + p2.timeHungryMillis + p3.timeHungryMillis
	hungerAvg := sum / 4

	sum = p0.timeThinkingMillis + p1.timeThinkingMillis + p2.timeThinkingMillis + p3.timeThinkingMillis
	thinkAvg := sum / 4

	logger.Printf(`

	On average the time counts for each philospher are:
	eating=%d ms, hungry=%d ms, thinking=%d ms
	`, eatAvg, hungerAvg, thinkAvg)

	os.Exit(0)
}
